/**
 * Created by vitalik on 20.05.17.
 */

(function () {
    'use strict';


    angular.module("mainApp")
        .config(function($stateProvider, $urlRouterProvider,){


            $stateProvider

                .state('welcome', {
                    url: '/welcome',
                    controller: 'welcomeController',
                    controllerAs: '$welcome',
                    templateUrl: 'templates/welcom_page.html'
                })
                
                .state('articleList', {
                    url: '/articles',
                    controller: 'ArticleListController',
                    controllerAs: '$articleList',
                    templateUrl: 'templates/article.html'
                })

                .state('article', {
                    url: '/article/:article_link',
                    controller: 'ArticleController',
                    controllerAs: '$article',
                    templateUrl: 'templates/article_list.html'
                })

                // .state('createArticle', {
                //     url: '/create/article',  
                //     controller: 'ArticleCreateController',
                //     controllerAs: '$create',
                //     templateUrl: 'templates/article_create.html'
                // })

                // .state('updateArticle', {
                //     url: '/update/article/:id',
                //     params: {
                //         state: "updateArticle"
                //     },
                //     controller: 'ArticleCreateController',
                //     controllerAs: '$create',
                //     templateUrl: 'templates/article_create.html'
                // });

            $urlRouterProvider.when('/', '/welcome');
            $urlRouterProvider.when('', '/welcome');
        })
})();
